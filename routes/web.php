<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PageController@home");

Route::get('/about', "PageController@about");

Route::get('tour/{tour}',"PageController@tour")->name("tour");
Route::get('/tours',"PageController@tours")->name('tours');


Route::get('/gallery', "PageController@gallery");

Route::get('/cars',  "PageController@cars")->name('cars');

Route::get('/car/{car}', "PageController@car" )->name("car");

//Set Application control panel language
Route::get('/locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
    \App::setLocale($locale);
    return redirect()->back();
})->name('setLocale');