@extends('common.layout')

@push('css')

@endpush

@section('content')
    <main class="gallery">
        <div class="images-container">
            @foreach(json_decode($gallery->images) as $item)
            <a href="{{ asset('storage/uploads/files/'.$item) }}" data-lightbox="roadtrip"
               class="image">
                <img src="{{ asset('storage/uploads/files/'.$item) }}" alt="">
            </a>
            @endforeach

        </div>
    </main>
@endsection

@push('scripts')

@endpush