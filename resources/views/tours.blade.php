@extends('common.layout')

@push('css')

@endpush

@section('content')
    <main class="tours">
        <div class="image-container"
             style="background-image: url('http://www.cookiesound.com/wp-content/uploads/2014/03/tongariro-alpine-crossing-emeral-lakes-new-zealand.jpg')">
            <div class="background-overlay"></div>
        </div>
        <div class="tours-container">
            <div class="tours-title">
                <h4>@lang('Our Tours')</h4>
                <p>@lang('Tours General Description')</p>
            </div>
            @foreach($tours as $tour)
            <div class="tour-block"
                 style="background-image: url({{ asset('storage/uploads/tours/'.$tour->outer_image) }})">
                <a href="{{ route("tour",$tour->id) }}">
                    <div class="background-overlay"></div>
                    <div class="title">
                        <h3>{{ $tour->name }}</h3>
                    </div>
                    <div class="duration">
                        <div class="icon">
                            @include('svg.clock')
                        </div>
                        <span>{{ count((array)json_decode($tour->days)) }} @lang("days")</span>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </main>
@endsection

@push('scripts')

@endpush