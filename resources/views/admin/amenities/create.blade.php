@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                    <li class="breadcrumb-item">
                <a href="{{ route("amenities.index") }}">Amenities</a>
            </li>
                <li class="breadcrumb-item active">Amenities</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Amenities</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("amenities.store") }}" method="post"
                                          enctype="multipart/form-data">
                                                                                @csrf
                                        @php
    if(isset($data)){
        $value = $data->name;
        $value = key_exists('name',old()) ? old('name') : $value;
    } else{
        $value = old('name');
    }
@endphp


<div class="form-group row ">
        <label class="col-md-3 col-form-label" for="text-input">Name</label>
        <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="string" name="name" placeholder="Name">
        @if($errors->get('name'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->description;
        $value = key_exists('description',old()) ? old('description') : $value;
    } else{
        $value = old('description');
    }
@endphp

<div class="form-group row">
        <label class="col-md-3 col-form-label" for="text-input">Description</label>
        <div class="col-md-9">
        <textarea rows="5" cols="50" class="form-control" name="description" placeholder="Description">{{ $value }}</textarea>
        @if($errors->get('description'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('description') }}</div>
        @endif
    </div>
</div>
                                        <input type="hidden" value="\App\Modules\Amenities\AmenitiesRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection