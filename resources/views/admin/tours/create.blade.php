@extends('betterfly::admin.common.layout')

@section('content')

    <main class="main">

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route("tours.index") }}">Tours</a>
            </li>
            <li class="breadcrumb-item active">Tours</li>
        </ol>

        @if(\Session::get('status'))
            <div class="container-fluid">
                <div id="ui-view">
                    <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
                </div>
            </div>
        @endif
        <div class="container-fluid">
            <div id="ui-view">
                <div>
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Tours</strong>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal"
                                              action="{{ route("tours.store") }}" method="post"
                                              enctype="multipart/form-data">
                                            @csrf
                                            @php
                                                if(isset($data)){
                                                    $value = $data->name;
                                                    $value = key_exists('name',old()) ? old('name') : $value;
                                                } else{
                                                    $value = old('name');
                                                }
                                            @endphp


                                            <div class="form-group row ">
                                                <label class="col-md-3 col-form-label" for="text-input">Tour
                                                    Name</label>
                                                <div class="col-md-9">
                                                    <input class="form-control" value="{{ $value }}" type="text"
                                                           name="name" placeholder="Tour Name">
                                                    @if($errors->get('name'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('name') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            @php
                                                if(isset($data)){
                                                $value = $data->text;
                                                $value = key_exists('text',old()) ? old('text') : $value;
                                                } else{
                                                $value = old('text');
                                                }
                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="text-input">text</label>
                                                <div class="col-md-9">
        <textarea id="ckEditor_1" name="text">
            {{ $value }}
        </textarea>
                                                    @if($errors->get('text'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('text') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            @push('scripts')


                                                <script>
                                                    loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

                                                    loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_1);

                                                    function loadckEditor_1() {
                                                        loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
                                                            $("textarea#ckEditor_1").ckeditor({
                                                                extraPlugins: "image2",
                                                                filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
                                                                filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
                                                            });
                                                        });
                                                    }
                                                </script>
                                            @endpush@php
                                                if(isset($data)){
                                                    $files =  json_decode($data->outer_image) ? json_decode($data->outer_image) : ($data->outer_image && $data->outer_image !== 'none' ?  [$data->outer_image]: []);
                                                    $value =  $data->outer_image;
                                                }else{
                                                    $value = null;
                                                    $files = [];
                                                }

                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="fileField_2">Select Outer
                                                    Image</label>
                                                <div class="col-md-9">
                                                    <input for="outer_image" name="fileField_2" id="fileField_2"
                                                           type="file">
                                                    <input type="hidden"
                                                           name="outer_image" {{ $value ? "value=".$value : "value=none" }}>
                                                    <br>
                                                    <br>
                                                    @foreach($files as $key => $file)
                                                        <div class="preview-container">
                                                            <img class="old file-preview {{ @strpos(mime_content_type('storage/uploads/tours/'.$file),'image') !== 0 ? 'filetype-file' : 'filetype-image' }}"
                                                                 data-src="{{ 'storage/uploads/tours/'.$file }}"
                                                                 src="{{ 'storage/uploads/tours/'.$file }}"
                                                                 height="150">
                                                            <a data-index="{{ $key }}" href="javascript:;"
                                                               class="remove-image">
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @push('scripts')
                                                <script>
                                                    loadCss(['../vendor/betterfly/plugins/filePlugin/filePlugin.css']);
                                                    loadScript(['../vendor/betterfly/plugins/filePlugin/filePlugin.js'], loadfileField_2);

                                                    function loadfileField_2() {
                                                        $('input#fileField_2').filePlugin({
                                                            maxCount: 1,
                                                            mimeTypes: ["jpg", "jpeg", "png", "svg",],
                                                            folder: "tours",
                                                            thumbs: [],
                                                            required: false
                                                        });
                                                    }
                                                </script>
                                            @endpush@php
                                                if(isset($data)){
                                                    $files =  json_decode($data->inner_image) ? json_decode($data->inner_image) : ($data->inner_image && $data->inner_image !== 'none' ?  [$data->inner_image]: []);
                                                    $value =  $data->inner_image;
                                                }else{
                                                    $value = null;
                                                    $files = [];
                                                }

                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="fileField_3">Select Inner
                                                    Image</label>
                                                <div class="col-md-9">
                                                    <input for="inner_image" name="fileField_3" id="fileField_3"
                                                           type="file">
                                                    <input type="hidden"
                                                           name="inner_image" {{ $value ? "value=".$value : "value=none" }}>
                                                    <br>
                                                    <br>
                                                    @foreach($files as $key => $file)
                                                        <div class="preview-container">
                                                            <img class="old file-preview {{ @strpos(mime_content_type('storage/uploads/tours/'.$file),'image') !== 0 ? 'filetype-file' : 'filetype-image' }}"
                                                                 data-src="{{ 'storage/uploads/tours/'.$file }}"
                                                                 src="{{ 'storage/uploads/tours/'.$file }}"
                                                                 height="150">
                                                            <a data-index="{{ $key }}" href="javascript:;"
                                                               class="remove-image">
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @push('scripts')
                                                <script>
                                                    loadCss(['../vendor/betterfly/plugins/filePlugin/filePlugin.css']);
                                                    loadScript(['../vendor/betterfly/plugins/filePlugin/filePlugin.js'], loadfileField_3);

                                                    function loadfileField_3() {
                                                        $('input#fileField_3').filePlugin({
                                                            maxCount: 1,
                                                            mimeTypes: ["jpg", "jpeg", "png", "svg",],
                                                            folder: "tours",
                                                            thumbs: [],
                                                            required: false
                                                        });
                                                    }
                                                </script>
                                            @endpush@php
                                                if(isset($data)){
                                                    $value = is_a($data->amenities,'Illuminate\Database\Eloquent\Collection') ? $data->amenities->pluck('id')->toArray() : $data->amenities;
                                                    $value = key_exists('amenities',old()) ? old('amenities') : $value;
                                                } else{
                                                    $value = old('amenities');
                                                }
                                            @endphp


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="selectField_4">Select
                                                    Amenities</label>
                                                <div class="col-md-9">
                                                    <select class="dd col-lg-12 form-control" id="selectField_4"
                                                            name="amenities[]" multiple>
                                                        @php
                                                            $amenities = getAmenities();
                                                        @endphp
                                                        @foreach($amenities as $option)
                                                            <option {{ is_array($value) ? (in_array($option->id,$value) ? 'selected' : '' ) : ($value == $option->id ? 'selected' : '' )  }} value="{{ $option->id }}">
                                                                {{ $option->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->get('amenities'))
                                                        <br>
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('amenities') }}</div>
                                                    @endif
                                                </div>
                                            </div>


                                            @push('scripts')

                                                <script>

                                                    loadCss(['../vendor/betterfly/plugins/selectPlugin/select2.min.css']);
                                                    loadScript(['../vendor/betterfly/plugins/selectPlugin/select2.min.js'], loadselectField_4);

                                                    function loadselectField_4() {
                                                        $("select#selectField_4").select2();
                                                    }

                                                </script>

                                            @endpush@php
                                                if(isset($data)){
                                                    $value = json_decode($data->days);
                                                    $value = key_exists('days',old()) ? json_decode(old('days')) : $value;
                                                } else{
                                                    $value = json_decode(old('days')) ? json_decode(old('days')) : [(object)[0=> "", 1=> "",2 =>""] ];
                                                }
                                            @endphp

                                            <div class="row">
                                                <label class="col-md-3 col-form-label" for="multiField_5">Days
                                                    Description</label>

                                                <div class="col-md-9 p-0" id="multiField_5">
                                                    <input hidden type="text" name="days" class="multiField_5_input">
                                                    <div class="col-md-12 ">
                                                        <button type="button" id="btnAdd-multiField_5"
                                                                class="btn btn-primary">Add section
                                                        </button>
                                                    </div>
                                                    @foreach($value as $key => $items)
                                                        <div class="group col-md-12 row mt-3 mb-3">
                                                            <div class="col-md-6">
                                                                @php
                                                                    $value = $items->{0}
                                                                @endphp


                                                                <div class="form-group row ">
                                                                    <div class="col-md-12">
                                                                        <input class="form-control" value="{{ $value }}"
                                                                               type="text" name="" placeholder="Day">
                                                                        @if($errors->get(''))
                                                                            <br>
                                                                            <div class="alert alert-danger"
                                                                                 role="alert">{{ $errors->first('') }}</div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                @php
                                                                    $value = $items->{1}
                                                                @endphp


                                                                <div class="form-group row ">
                                                                    <div class="col-md-12">
                                                                        <input class="form-control" value="{{ $value }}"
                                                                               type="text" name=""
                                                                               placeholder="Destination">
                                                                        @if($errors->get(''))
                                                                            <br>
                                                                            <div class="alert alert-danger"
                                                                                 role="alert">{{ $errors->first('') }}</div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                @php
                                                                    $value = $items->{2}
                                                                @endphp

                                                                <div class="form-group row">
                                                                    <div class="col-md-12">
                                                                        <textarea rows="5" cols="50"
                                                                                  class="form-control" name=""
                                                                                  placeholder="Description">{{ $value }}</textarea>
                                                                        @if($errors->get(''))
                                                                            <br>
                                                                            <div class="alert alert-danger"
                                                                                 role="alert">{{ $errors->first('') }}</div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 mt-2">
                                                                <button type="button" class="btn btn-danger btnRemove">
                                                                    Remove
                                                                </button>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @push('scripts')
                                                <script>
                                                    loadScript(['../vendor/betterfly/plugins/multifield/jquery.multifield.min.js'], loadmultiField_5);

                                                    function loadmultiField_5() {
                                                        $('#multiField_5').multifield({
                                                            section: '.group',
                                                            btnAdd: '#btnAdd-multiField_5',
                                                            btnRemove: '.btnRemove',
                                                        });

                                                        $('form .btn-sm.btn-success').click(function (e) {
                                                            e.preventDefault();
                                                            var data = {};
                                                            for (i = 0; i < $('#multiField_5 .group').length; i++) {
                                                                var parentEl = $($('#multiField_5 .group')[i]);
                                                                var inputs = parentEl.find('.form-control');
                                                                data[i] = {};
                                                                $.each(inputs, function (k, v) {
                                                                    data[i][k] = $(v).val()
                                                                });
                                                            }
                                                            data = JSON.stringify(data);
                                                            $('.multiField_5_input').val(data);

                                                            $(this).parents('form').submit()
                                                        })
                                                    }
                                                </script>
                                            @endpush
                                            <input type="hidden" value="\App\Modules\Tours\ToursRequest"
                                                   name="request_name_space">
                                            <div class=" text-right">
                                                <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                    <i class="fa fa-dot-circle-o"></i> Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection