<li class="nav-item">
    <a class="nav-link" href="{{ route('tours.index') }}">
        <i class="nav-icon icon-pie-chart"></i> Tours</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('amenities.index') }}">
        <i class="nav-icon icon-pie-chart"></i> Amenities</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('about.edit',1) }}">
        <i class="nav-icon icon-pie-chart"></i> About</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('gallery.edit',1) }}">
        <i class="nav-icon icon-pie-chart"></i> Gallery</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('cars.index') }}">
        <i class="nav-icon icon-pie-chart"></i> Cars</a>
</li>
