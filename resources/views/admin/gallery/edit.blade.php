@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                <li class="breadcrumb-item active">Gallery</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Gallery</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("gallery.update",[$data->id]) }}" method="post"
                                          enctype="multipart/form-data">
                                                                                    @method('PUT')
                                                                                @csrf
                                        @php
    if(isset($data)){
        $files =  json_decode($data->images) ? json_decode($data->images) : ($data->images && $data->images !== 'none' ?  [$data->images]: []);
        $value =  $data->images;
    }else{
        $value = null;
        $files = [];
    }

@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="fileField_0">Images</label>
    <div class="col-md-9">
        <input for="images" name="fileField_0" id="fileField_0" type="file">
        <input type="hidden" name="images" {{ $value ? "value=".$value : "value=none" }}>
        <br>
        <br>
        @foreach($files as $key => $file)
            <div class="preview-container">
                <img class="old file-preview {{ @strpos(mime_content_type('storage/uploads/files/'.$file),'image') !== 0 ? 'filetype-file' : 'filetype-image' }}" data-src="{{ 'storage/uploads/files/'.$file }}" src="{{ 'storage/uploads/files/'.$file }}" height="150">
                <a data-index="{{ $key }}" href="javascript:;" class="remove-image">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        @endforeach
    </div>
</div>
@push('scripts')
<script>
  loadCss(['../vendor/betterfly/plugins/filePlugin/filePlugin.css']);
  loadScript(['../vendor/betterfly/plugins/filePlugin/filePlugin.js'], loadfileField_0);

  function loadfileField_0() {
    $('input#fileField_0').filePlugin({
      maxCount: 999999,
      mimeTypes: ["jpg","jpeg","png","svg",],
      folder: "files",
      thumbs: [],
      required: true
    });
  }
</script>
@endpush
                                        <input type="hidden" value="\App\Modules\Gallery\GalleryRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection