@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                    <li class="breadcrumb-item">
                <a href="{{ route("cars.index") }}">Cars</a>
            </li>
                <li class="breadcrumb-item active">Cars</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Cars</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("cars.update",[$data->id]) }}" method="post"
                                          enctype="multipart/form-data">
                                                                                    @method('PUT')
                                                                                @csrf
                                        @php
    if(isset($data)){
        $value = $data->name;
        $value = key_exists('name',old()) ? old('name') : $value;
    } else{
        $value = old('name');
    }
@endphp


<div class="form-group row ">
        <label class="col-md-3 col-form-label" for="text-input">Car Name</label>
        <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="text" name="name" placeholder="Car Name">
        @if($errors->get('name'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
        @endif
    </div>
</div>
@php
if(isset($data)){
$value = $data->text;
$value = key_exists('text',old()) ? old('text') : $value;
} else{
$value = old('text');
}
@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="text-input">text</label>
    <div class="col-md-9">
        <textarea id="ckEditor_1" name="text">
            {{ $value }}
        </textarea>
        @if($errors->get('text'))
        <br>
        <div class="alert alert-danger" role="alert">{{ $errors->first('text') }}</div>
        @endif
    </div>
</div>

@push('scripts')


<script>
  loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

  loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_1);

  function loadckEditor_1() {
    loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
      $("textarea#ckEditor_1").ckeditor({
        extraPlugins: "image2",
        filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
        filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
      });
    });
  }
</script>
@endpush@php
    if(isset($data)){
        $files =  json_decode($data->outer_image) ? json_decode($data->outer_image) : ($data->outer_image && $data->outer_image !== 'none' ?  [$data->outer_image]: []);
        $value =  $data->outer_image;
    }else{
        $value = null;
        $files = [];
    }

@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="fileField_2">Select Outer Image</label>
    <div class="col-md-9">
        <input for="outer_image" name="fileField_2" id="fileField_2" type="file">
        <input type="hidden" name="outer_image" {{ $value ? "value=".$value : "value=none" }}>
        <br>
        <br>
        @foreach($files as $key => $file)
            <div class="preview-container">
                <img class="old file-preview {{ @strpos(mime_content_type('storage/uploads/cars/'.$file),'image') !== 0 ? 'filetype-file' : 'filetype-image' }}" data-src="{{ 'storage/uploads/cars/'.$file }}" src="{{ 'storage/uploads/cars/'.$file }}" height="150">
                <a data-index="{{ $key }}" href="javascript:;" class="remove-image">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        @endforeach
    </div>
</div>
@push('scripts')
<script>
  loadCss(['../vendor/betterfly/plugins/filePlugin/filePlugin.css']);
  loadScript(['../vendor/betterfly/plugins/filePlugin/filePlugin.js'], loadfileField_2);

  function loadfileField_2() {
    $('input#fileField_2').filePlugin({
      maxCount: 1,
      mimeTypes: ["jpg","jpeg","png","svg",],
      folder: "cars",
      thumbs: [],
      required: false
    });
  }
</script>
@endpush@php
    if(isset($data)){
        $files =  json_decode($data->inner_image) ? json_decode($data->inner_image) : ($data->inner_image && $data->inner_image !== 'none' ?  [$data->inner_image]: []);
        $value =  $data->inner_image;
    }else{
        $value = null;
        $files = [];
    }

@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="fileField_3">Select Inner Image</label>
    <div class="col-md-9">
        <input for="inner_image" name="fileField_3" id="fileField_3" type="file">
        <input type="hidden" name="inner_image" {{ $value ? "value=".$value : "value=none" }}>
        <br>
        <br>
        @foreach($files as $key => $file)
            <div class="preview-container">
                <img class="old file-preview {{ @strpos(mime_content_type('storage/uploads/cars/'.$file),'image') !== 0 ? 'filetype-file' : 'filetype-image' }}" data-src="{{ 'storage/uploads/cars/'.$file }}" src="{{ 'storage/uploads/cars/'.$file }}" height="150">
                <a data-index="{{ $key }}" href="javascript:;" class="remove-image">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        @endforeach
    </div>
</div>
@push('scripts')
<script>
  loadCss(['../vendor/betterfly/plugins/filePlugin/filePlugin.css']);
  loadScript(['../vendor/betterfly/plugins/filePlugin/filePlugin.js'], loadfileField_3);

  function loadfileField_3() {
    $('input#fileField_3').filePlugin({
      maxCount: 1,
      mimeTypes: ["jpg","jpeg","png","svg",],
      folder: "cars",
      thumbs: [],
      required: false
    });
  }
</script>
@endpush@php
    if(isset($data)){
        $value = is_a($data->amenities,'Illuminate\Database\Eloquent\Collection') ? $data->amenities->pluck('id')->toArray() : $data->amenities;
        $value = key_exists('amenities',old()) ? old('amenities') : $value;
    } else{
        $value = old('amenities');
    }
@endphp



<div class="form-group row">
    <label class="col-md-3 col-form-label" for="selectField_4">Select Amenities</label>
    <div class="col-md-9">
        <select  class="dd col-lg-12 form-control" id="selectField_4"
                name="amenities[]" multiple>
                                        @php
                    $amenities = getAmenities()
                @endphp
                                                    @foreach($amenities as $option)
                        <option {{ is_array($value) ? (in_array($option->id,$value) ? 'selected' : '' ) : ($value == $option->id ? 'selected' : '' )  }} value="{{ $option->id }}">
                            {{ $option->name }}
                        </option>
                    @endforeach
                        </select>
        @if($errors->get('amenities'))
            <br>
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('amenities') }}</div>
        @endif
    </div>
</div>


@push('scripts')

<script>

  loadCss(['../vendor/betterfly/plugins/selectPlugin/select2.min.css']);
  loadScript(['../vendor/betterfly/plugins/selectPlugin/select2.min.js'], loadselectField_4);

  function loadselectField_4() {
    $("select#selectField_4").select2();
  }

</script>

@endpush@php
    if(isset($data)){
        $value = $data->model;
        $value = key_exists('model',old()) ? old('model') : $value;
    } else{
        $value = old('model');
    }
@endphp


<div class="form-group row ">
        <label class="col-md-3 col-form-label" for="text-input">Car Model</label>
        <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="model" placeholder="Car Model">
        @if($errors->get('model'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('model') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->price;
        $value = key_exists('price',old()) ? old('price') : $value;
    } else{
        $value = old('price');
    }
@endphp


<div class="form-group row ">
        <label class="col-md-3 col-form-label" for="text-input">Car Price</label>
        <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="price" placeholder="Car Price">
        @if($errors->get('price'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('price') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->pasengers;
        $value = key_exists('pasengers',old()) ? old('pasengers') : $value;
    } else{
        $value = old('pasengers');
    }
@endphp


<div class="form-group row ">
        <label class="col-md-3 col-form-label" for="text-input">Passengers</label>
        <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="pasengers" placeholder="Passengers">
        @if($errors->get('pasengers'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('pasengers') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->luggage;
        $value = key_exists('luggage',old()) ? old('luggage') : $value;
    } else{
        $value = old('luggage');
    }
@endphp


<div class="form-group row ">
        <label class="col-md-3 col-form-label" for="text-input">Luggage Space</label>
        <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="luggage" placeholder="Luggage Space">
        @if($errors->get('luggage'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('luggage') }}</div>
        @endif
    </div>
</div>

                                        <input type="hidden" value="\App\Modules\Cars\CarsRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection