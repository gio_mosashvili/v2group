@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                <li class="breadcrumb-item active">About</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>About</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("about.update",[$data->id]) }}" method="post"
                                          enctype="multipart/form-data">
                                                                                    @method('PUT')
                                                                                @csrf
                                        @php
    if(isset($data)){
        $files =  json_decode($data->image) ? json_decode($data->image) : ($data->image && $data->image !== 'none' ?  [$data->image]: []);
        $value =  $data->image;
    }else{
        $value = null;
        $files = [];
    }

@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="fileField_0">Image</label>
    <div class="col-md-9">
        <input for="image" name="fileField_0" id="fileField_0" type="file">
        <input type="hidden" name="image" {{ $value ? "value=".$value : "value=none" }}>
        <br>
        <br>
        @foreach($files as $key => $file)
            <div class="preview-container">
                <img class="old file-preview {{ @strpos(mime_content_type('storage/uploads/about/'.$file),'image') !== 0 ? 'filetype-file' : 'filetype-image' }}" data-src="{{ 'storage/uploads/about/'.$file }}" src="{{ 'storage/uploads/about/'.$file }}" height="150">
                <a data-index="{{ $key }}" href="javascript:;" class="remove-image">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        @endforeach
    </div>
</div>
@push('scripts')
<script>
  loadCss(['../vendor/betterfly/plugins/filePlugin/filePlugin.css']);
  loadScript(['../vendor/betterfly/plugins/filePlugin/filePlugin.js'], loadfileField_0);

  function loadfileField_0() {
    $('input#fileField_0').filePlugin({
      maxCount: 1,
      mimeTypes: ["jpg","jpeg","png","svg",],
      folder: "about",
      thumbs: [],
      required: false
    });
  }
</script>
@endpush@php
if(isset($data)){
$value = $data->text;
$value = key_exists('text',old()) ? old('text') : $value;
} else{
$value = old('text');
}
@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="text-input">Text</label>
    <div class="col-md-9">
        <textarea id="ckEditor_1" name="text">
            {{ $value }}
        </textarea>
        @if($errors->get('text'))
        <br>
        <div class="alert alert-danger" role="alert">{{ $errors->first('text') }}</div>
        @endif
    </div>
</div>

@push('scripts')


<script>
  loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

  loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_1);

  function loadckEditor_1() {
    loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
      $("textarea#ckEditor_1").ckeditor({
        extraPlugins: "image2",
        filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
        filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
      });
    });
  }
</script>
@endpush
                                        <input type="hidden" value="\App\Modules\About\AboutRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection