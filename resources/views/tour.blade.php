@extends('common.layout')

@push('css')

@endpush

@section('content')
    <main class="tour">
        <div class="image-container" style="background-image: url({{ asset('storage/uploads/tours/'.$tour->inner_image) }})">
            <div class="background-overlay"></div>
        </div>
        <div class="tour-container">
            <div class="tour-content">
                <div class="title">
                    <h2>{{ $tour->name }}</h2>
                </div>
                <div class="description">
                    <div class="text-block">
                        <h3 class="title">@lang('overview')</h3>
                        <p class="description">{!! $tour->text  !!}</p>
                    </div>
                </div>
                @if(json_decode($tour->days))
                <div class="plan-container">
                    @foreach(json_decode($tour->days) as $day)
                    <div class="plan">
                        <div class="plan-header">
                            <span class="duration">
                                {{ $day->{0} }}
                            </span>
                            <div class="plan-title">
                                {{ $day->{1} }}
                            </div>
                            <span class="icon">
                                @include('svg.plus')
                            </span>
                        </div>
                        <div class="plan-description">
                            {!! $day->{2} !!}
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
                @if($tour->amenities->count())
                <div class="amenities">
                    <div class="title">
                        <h2>@lang('Amenities')</h2>
                    </div>
                    @foreach($tour->amenities as $item)
                    <div class="amenity-block">
                        <div class="icon">
                            @include('svg.checkmark')
                        </div>
                        <div class="right">
                            <span>{{ $item->name }}</span>
                            <p>{{ $item->description }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
                <a href="javascritp:;" class="share fbshare" data-url="{{ route('tour',$tour->id) }}">
                    <div class="fb-share">
                        @include('svg.fb')
                    </div>
                    <span>@lang('Share')</span>
                </a>
                <div class="view-all-tours">
                    <a href="{{ route("tours") }}" class="view-all">
                        <span>@lang('View All Tours')</span>
                    </a>
                </div>
            </div>
        </div>
    </main>
@endsection

@push('scripts')

@endpush