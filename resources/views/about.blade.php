@extends('common.layout')

@push('css')

@endpush

@section('content')
    <main class="about">
        <div class="image-container"
             style="background-image: url({{ asset('storage/uploads/about/'.$about->image) }})">
            <div class="background-overlay"></div>
        </div>
        <div class="about-block">
            <div class="text-block">
               {!! $about->text !!}
            </div>
        </div>
        {{--<div class="map">--}}
            {{--<div class="map-title">--}}
                {{--<h4>Explore Our Travel Map</h4>--}}
                {{--<p>One lifetime is not enough to see it all.--}}
                    {{--We do our best to explore as many places as possible.</p>--}}
            {{--</div>--}}
            {{--<div class="map-container">--}}
                {{--<iframe src="https://snazzymaps.com/embed/83947" width="100%" height="100%"--}}
                        {{--style="border:none;"></iframe>--}}
            {{--</div>--}}
        {{--</div>--}}
    </main>
@endsection

@push('scripts')

@endpush