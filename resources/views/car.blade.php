@extends('common.layout')

@push('css')

@endpush

@section('content')
    <main class="car">
        <div class="image-container"
             style="background-image: url({{ asset('storage/uploads/cars/'.$car->inner_image) }})">
            <div class="background-overlay"></div>
        </div>
        <div class="car-container">
            <div class="car-content">
                <div class="title">
                    <h2>{{ $car->name }}</h2>
                </div>
                <div class="description">
                    <div class="text-block">
                        <h3 class="title">@lang('overview')</h3>
                        <p class="description">{!! $car->text !!}</p>
                    </div>
                </div>
                <div class="car-info">
                    <div class="title">
                        <h2>@lang('Car info')</h2>
                    </div>
                    <div class="info-block">
                        <div class="right">
                            <span>@lang('Model')</span>
                            <p>{{ $car->model }}</p>
                        </div>
                    </div>
                    {{--<div class="info-block">--}}
                        {{--<div class="right">--}}
                            {{--<span>Doors</span>--}}
                            {{--<p>4</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="info-block">
                        <div class="right">
                            <span>@lang('Pasengers')</span>
                            <p>{{ $car->pasengers }}</p>
                        </div>
                    </div>
                    <div class="info-block">
                        <div class="right">
                            <span>@lang('Luggage')</span>
                            <p>{{ $car->luggage }}</p>
                        </div>
                    </div>
                    {{--<div class="info-block">--}}
                        {{--<div class="right">--}}
                            {{--<span>Transmission</span>--}}
                            {{--<p>Automatic</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="info-block">--}}
                        {{--<div class="right">--}}
                            {{--<span>Air Conditioning</span>--}}
                            {{--<p>Dual Zone</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="amenities">
                    <div class="title">
                        <h2>@lang('Amenities')</h2>
                    </div>
                    @foreach($car->amenities as $item)
                        <div class="amenity-block">
                            <div class="icon">
                                @include('svg.checkmark')
                            </div>
                            <div class="right">
                                <span>{{ $item->name }}</span>
                                <p>{{ $item->description }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="javascritp:;" class="share fbshare" data-url="{{ route('car',$car->id) }}">
                    <div class="fb-share">
                        @include('svg.fb')
                    </div>
                    <span>@lang('Share')</span>
                </a>
                <div class="view-all-car">
                    <a href="{{ route('cars') }}" class="view-all">
                        <span>@lang('View All Cars')</span>
                    </a>
                </div>
            </div>
        </div>
    </main>
@endsection

@push('scripts')

@endpush