@extends('common.layout')

@push('css')

@endpush

@section('content')
    <main class="cars">
        <div class="image-container"
             style="background-image: url('http://www.cookiesound.com/wp-content/uploads/2014/03/tongariro-alpine-crossing-emeral-lakes-new-zealand.jpg')">
            <div class="background-overlay"></div>
        </div>
        <div class="cars-container">
            <div class="cars-title">
                <h4>@lang('Our Cars')</h4>
                <p>@lang('Cars General Description')</p>
            </div>
            @foreach($cars as $car)
            <div class="car-block"
                 style="background-image: url({{ asset('storage/uploads/cars/'.$car->outer_image) }})">
                <a href="{{ route('car',$car->id) }}">
                    <div class="background-overlay"></div>
                    <div class="title">
                        <h3>{{ $car->name }}</h3>
                    </div>
                    <div class="carseat">
                        <div class="icon">
                            @include('svg.carseat')
                        </div>
                        <span>{{ $car->pasengers }}</span>
                    </div>
                </a>
            </div>
            @endforeach

        </div>
    </main>
@endsection

@push('scripts')

@endpush