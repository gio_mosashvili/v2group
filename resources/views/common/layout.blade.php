<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    @stack('css')
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body class="preload-loading preload">
    @include('common.header')

    @yield('content')

    @include('common.footer')

    @stack('scripts')
    <script src="{{asset('js/app.js')}}"></script>
	{{--<div class="loader"></div>--}}
</body>
</html>
