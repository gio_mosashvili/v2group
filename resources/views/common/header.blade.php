<header>
    <div class="lang">
        <div class="active-language"></div>
        <div class="dropdown-container">
            <a href="{{ route('setLocale',"en") }}" class="en" data-lang="/en" data-name="en">en</a>
            <a href="{{ route('setLocale',"ka") }}" class="ka" data-lang="/ka" data-name="ka">ka</a>
            <a href="{{ route('setLocale',"ru") }}" data-lang="/ru" data-name="ru">ru</a>
        </div>
    </div>
    <div class="burger-menu">
        <span class="line line1"></span>
        <span class="line line2"></span>
        <span class="line line3"></span>
    </div>
    <div class="menu">
        <nav>
            <li><a href="/">Home</a></li>
            <li><a href="/tours">Tours</a></li>
            <li><a href="/cars">Cars</a></li>
            <li><a href="/gallery">Gallery</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/#contact">Contact</a></li>
        </nav>
    </div>
</header>
