<footer>
    <div class="footer-container">
        <div class="logo">
            <span>LOGO</span>
        </div>
        <div class="footer-menu">
            <nav>
                <li><a href="/">Home</a></li>
                <li><a href="/tours">Tours</a></li>
                <li><a href="/cars">Cars</a></li>
                <li><a href="/gallery">Gallery</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/">Contact</a></li>
            </nav>
        </div>
        <div class="socials">
            <a href="" target="_blank" class="fb">
                @include('svg.fb')
            </a>
            <a href="" target="_blank" class="instagram">
                @include('svg.instagram')
            </a>
            <a href="" target="_blank" class="youtube">
                @include('svg.youtube')
            </a>
        </div>
        <div class="copyright">
            &copy; 2019 all rights reserved.
        </div>
    </div>
</footer>
