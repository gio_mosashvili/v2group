@extends('common.layout')

@push('css')

@endpush

@section('content')
    <main class="home">
        <div class="slider-container">
            <div class="slider home-slider">
                <div class="slide">
                    <div class="slick-background">
                        <div id="player"></div>
                    </div>
                    <div class="slide-description">
                        {{--<div class="location">--}}
                        {{--<span>Asia,India</span>--}}
                        {{--</div>--}}
                        {{--<div class="title">--}}
                        {{--<h3>welcome to little tibet, welcome to ladakh, welcome to leh.</h3>--}}
                        {{--</div>--}}
                        {{--<a href="" class="read-more">--}}
                        {{--<span>read more</span>--}}
                        {{--</a>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="discover-more">
            <span>@lang('discover more')</span>
        </div>
        <div class="tours-container" id="tours">
            <div class="tours-title">
                {{--<span>TRAVEL WRITING</span>--}}
                <h4>@lang('Our Tours')</h4>
                <p>@lang('Tours General Description')</p>
            </div>
            <div class="tours-content">
                @foreach($tours as $tour)
                    <a href="{{ route("tour",$tour->id) }}" class="tour-block">
                        <div>
                            <div class="tour-image"
                                 style="background-image: url({{ asset('storage/uploads/tours/'.$tour->outer_image) }})">
                                <div class="image-background">
                                    <span>@lang('read more')</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="tour-description">
                                <div class="location">
                                    <span>{{ $tour->name }}</span>
                                </div>
                                <div class="title">
                                    <h4>{!! substr(strip_tags($tour->text),0,50)  !!} ...</h4>
                                </div>
                                {{--<div class="read-story">--}}
                                {{--<span>view more</span>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </a>
                @endforeach

            </div>
        </div>
        <div class="cars-container">
            <div class="cars-title">
                <h4>@lang('Our Cars')</h4>
                <p>@lang('Cars General Description')</p>
            </div>
            <div class="cars-content">

                @foreach($cars as $car)
                <a href="{{ route('car',$car->id) }}" class="car-block">
                    <div>
                        <div class="car-image"
                             style="background-image: url({{ asset('storage/uploads/cars/'.$car->outer_image) }})">
                            <div class="image-background">
                                <span>@lang('read more')</span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="car-description">
                            <div class="car-option">
                                <span class="title">@lang('Passengers'):</span>
                                <span class="answer">{{ $car->pasengers }}</span>
                            </div>
                            <div class="car-option">
                                <span class="title">@lang('Model'):</span>
                                <span class="answer">{{ $car->model }}</span>
                            </div>
                            <div class="car-option">
                                <span class="title">@lang('Price'):</span>
                                <span class="answer">{{ $car->price }}</span>
                            </div>
                        </div>
                    </div>
                </a>
                    @endforeach

            </div>
        </div>
        <div class="contact" id="contact">
            <div class="left">
                <div class="title">
                    <span>@lang('Contact Us')</span>
                </div>
                <input class="half-input" type="text" name="name" placeholder="@lang('Name')*" required>
                <input class="half-input" type="email" name="email" placeholder="@lang('Email')*" required>
                <textarea class="textarea" placeholder="@lang('Message')*" required></textarea>
                <button class="send-button">
                    <span>@lang('Send')</span>
                </button>
            </div>
            <div class="right">
                <div class="map-container">
                    <iframe width="100%" height="100%" id="gmap_canvas"
                            src="https://maps.google.com/maps?q=vaja%20pshavelas%20&t=&z=13&ie=UTF8&iwloc=&output=embed"></iframe>
                </div>
            </div>
        </div>
    </main>
@endsection

@push('scripts')
    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var player;

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                videoId: '@lang('video_id')',
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        function onPlayerReady(event) {
            event.target.setPlaybackQuality('highres');
            event.target.setLoop();
            event.target.playVideo();
        }

        function onPlayerStateChange(event) {
            event.target.setPlaybackQuality('highres');
            
            if (event.data == YT.PlayerState.ENDED || event.data == -1) {
                player.playVideo();
            }
        }
    </script>
@endpush