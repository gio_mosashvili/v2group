$(document).ready(function () {
  $(".home-slider").slick({
    slidesToShow: 1,
    arrows: true,
    fade: true,
    cssEase: 'linear',
    prevArrow: $('.slick-prev'),
    nextArrow: $('.slick-next'),
  });
  //discover me scroll
  $(".discover-more span").click(function () {
    $('html, body').animate({
      scrollTop: $("#tours").offset().top
    }, 1000);
  });
  //tour page
  $('.plan-header').click(function () {
    if ($(this).parent('.plan').hasClass("active")) {
      $(this).parent('.plan').removeClass('active');
    } else {
      $('.plan').removeClass('active');
      $(this).parent('.plan').addClass('active');
    }
  });




});
