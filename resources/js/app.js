window.$ = require('jquery');
window.JQuery = require('jquery');

require('./bootstrap');

import Slick from '../../node_modules/slick-carousel/slick/slick.js';
import lightBox from '../../node_modules/lightbox2/dist/js/lightbox-plus-jquery.min.js';

require ('./common/events');
require ('./common/header');
require ('./pages/general');
