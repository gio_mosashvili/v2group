$(document).ready(function () {
    $('.burger-menu').click(function () {
        $('header,main,footer').toggleClass('open');
        if($('header,main,footer').hasClass('open')){
            $('.active-language').removeClass('active')
            $('.dropdown-container').removeClass('active')
        }
    });
    var loc = window.location.href;
    $(".lang a").each(function () {
        if (loc.indexOf($(this).attr("data-lang")) != -1) {
            $(this).addClass("active");
            $('.active-language').text($(this).attr('data-name'))
        }
    })
    $('.lang').click(function () {
        $(this).children('.active-language').toggleClass('active')
        $('.dropdown-container').toggleClass('active')
    })

    $(window).scroll(function () {
        var scrollTop = $( window ).scrollTop();

        if(scrollTop == 0){
            $('header').removeClass('open')
            $('main').removeClass('open')
        }else if(scrollTop > 300 && !$('header').hasClass('open')){
            $('.burger-menu').click()
        }
    })
});