$(function(){
    $('a:not(.image,.fbshare)').click(function(e){
    	e.preventDefault();
    	$('body').fadeOut();
    	var link = $(this).attr('href');
    	setTimeout(function(){
	    	window.location = link
    	},500)
    })

	$('.fbshare').click(function(){
		var data = $(this).data();
		window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(data.url),"","width=600px, height=400px")
	})
});