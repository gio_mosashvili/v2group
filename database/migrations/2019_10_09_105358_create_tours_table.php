<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing tours
        Schema::create('tours', function (Blueprint $table) {
                            $table->increments('id');
                                        $table->text('outer_image')->nullable();
                            $table->text('inner_image')->nullable();
                            $table->string('amenities_id')->nullable();
                                        $table->timestamps();
            
                                                });
                    Schema::create('tours_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tours_id')->unsigned();
                            $table->text('name')->nullable();
                            $table->text('text')->nullable();
                            $table->text('days');
                        $table->string('locale')->index();


            $table->unique(['tours_id','locale']);
            $table->foreign('tours_id')->references('id')->on('tours')->onDelete('cascade');

            });
            }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
                Schema::dropIfExists('tours_translations');
                Schema::dropIfExists('tours');
    }
}
