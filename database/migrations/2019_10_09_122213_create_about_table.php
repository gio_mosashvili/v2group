<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing about
        Schema::create('about', function (Blueprint $table) {
                            $table->increments('id');
                                        $table->string('image')->nullable();
                                        $table->timestamps();
            
                    });
                    Schema::create('about_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('about_id')->unsigned();
                            $table->text('text');
                        $table->string('locale')->index();


            $table->unique(['about_id','locale']);
            $table->foreign('about_id')->references('id')->on('about')->onDelete('cascade');

            });
            }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
                Schema::dropIfExists('about_translations');
                Schema::dropIfExists('about');
    }
}
