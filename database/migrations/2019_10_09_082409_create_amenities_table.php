<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing amenities
        Schema::create('amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->nestedSet();
            $table->timestamps();

        });
        Schema::create('amenities_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amenities_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('locale')->index();


            $table->unique(['amenities_id', 'locale']);
            $table->foreign('amenities_id')->references('id')->on('amenities')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('amenities_translations');
        Schema::dropIfExists('amenities');
    }
}
