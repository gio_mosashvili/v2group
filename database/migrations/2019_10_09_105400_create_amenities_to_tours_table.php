<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmenitiesToToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
        public function up()
    {
        Schema::create('amenities_to_tours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id')->unsigned()->nullable();
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');

            $table->integer('amenity_id')->unsigned()->nullable();
            $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('amenities_to_tours');
    }
}
