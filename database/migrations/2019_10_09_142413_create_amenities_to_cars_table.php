<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmenitiesToCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
        public function up()
    {
        Schema::create('amenities_to_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->unsigned()->nullable();
            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');

            $table->integer('amenity_id')->unsigned()->nullable();
            $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('amenities_to_cars');
    }
}
