<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing cars
        Schema::create('cars', function (Blueprint $table) {
                            $table->increments('id');
                                        $table->text('outer_image')->nullable();
                            $table->text('inner_image')->nullable();
                            $table->string('amenities')->nullable();
                            $table->string('model')->nullable();
                            $table->string('price')->nullable();
                            $table->string('pasengers')->nullable();
                            $table->string('luggage')->nullable();
                                        $table->timestamps();
            
                                                });
                    Schema::create('cars_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cars_id')->unsigned();
                            $table->text('name')->nullable();
                            $table->text('text')->nullable();
                        $table->string('locale')->index();


            $table->unique(['cars_id','locale']);
            $table->foreign('cars_id')->references('id')->on('cars')->onDelete('cascade');

            });
            }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
                Schema::dropIfExists('cars_translations');
                Schema::dropIfExists('cars');
    }
}
