<?php

namespace App\Http\Controllers;

use App\Modules\About\About;
use App\Modules\Cars\Cars;
use App\Modules\Gallery\Gallery;
use App\Modules\Tours\Tours;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){
        $tours = Tours::orderBy("id","DESC")->take(4)->get();
        $cars = Cars::orderBy("id","DESC")->take(4)->get();

        return view('home')->with([ "tours" => $tours,"cars" => $cars]);
    }

    public function tours(){
        $tours = Tours::orderBy("id","DESC")->get();

        return view('tours')->with([ "tours" => $tours]);
    }

    public function tour(Tours $tour){
        return view('tour')->with([ "tour" => $tour]);
    }

    public function about(){
        $about = About::find(1);
        return view('about')->with([ "about" => $about]);
    }

    public function gallery(){
        $gallery = Gallery::find(1);
        return view('gallery')->with([ "gallery" => $gallery]);
    }

    public function cars(){
        $cars = Cars::orderBy("id","DESC")->get();

        return view('cars')->with([ "cars" => $cars]);
    }

    public function car(Cars $car){
        return view('car')->with([ "car" => $car]);
    }
}
