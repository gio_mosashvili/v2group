<?php

if (! function_exists('getAmenities')) {
    function getAmenities() {
        $amenities = \App\Modules\Amenities\Amenities::all();

       return $amenities;
    }
}
