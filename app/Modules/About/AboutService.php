<?php
namespace App\Modules\About;


use BetterFly\Skeleton\Services\BaseService;

class AboutService extends BaseService{

  public function __construct(AboutRepository $repository){
    parent::__construct($repository);
  }
}