<?php
namespace App\Modules\About;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class AboutTransformer extends BaseTransformerAbstract
{
    public function transform($about)
    {
        $data = [
            'id' => $about->id,
            'image' => $about->image,
            'text' => $about->text,
        ];

        return $data;
    }
}