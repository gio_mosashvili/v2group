<?php
namespace App\Modules\About;

    
use Illuminate\Database\Eloquent\Model;

class AboutTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["text"];


}