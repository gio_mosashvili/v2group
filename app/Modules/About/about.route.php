<?php
Route::resource('about', 'App\Modules\About\AboutController', [
    'names' => [
        'index' => 'about.index',
        'store' => 'about.store',
        'update' => 'about.update',
        'destroy' => 'about.delete'
    ]
]);
