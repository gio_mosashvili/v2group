<?php
namespace App\Modules\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AboutController extends Controller
{
    protected $aboutService;

    public function __construct(AboutService $aboutService)
    {
        $this->aboutService = $aboutService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

                $data = $this->aboutService->getList(['paginate' => 10]);
                if($request->ajax())
            return $data;
        else
            return view('admin.about.index')->with([
                'data' => $data,
                
]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.about.create')->with([
        
    ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @return  \Illuminate\Http\Response
    */
    public function store(AboutRequest $request){

                $item = $this->aboutService->create($request->input());
        if($request->ajax()){
            return $item;
        }else{
            \Session::flash('status','Successfully created');
            return redirect()->route('about.index');
        }
    }

    /**
    * Display the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function show($id){
        return $this->aboutService->getById($id) ?:
        response(['message' => 'Record not found']);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function edit(Request $request,$id){
        $data = $this->aboutService->getById($id);
        if($request->ajax())
            return $data;
        else
            return view('admin.about.edit')->with([
                'data' => $data,
    
        
        ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function update(AboutRequest $request, $id){
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->aboutService->update($data);

        if($request->ajax()){
            return $data;
        }else{
            \Session::flash('status','Successfully updated');
            return redirect()->route('about.edit',[$data->id]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return $this->aboutService->delete($id) ?
        response(['message' => 'Successfully deleted']) :
        response(['message' => 'Something went wrong']);
    }
}