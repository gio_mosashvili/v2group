<?php

namespace App\Modules\About;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';
    protected $fillable = ["image"];

    use Translatable;

    public $translationModel = 'App\Modules\About\AboutTranslation';
    public $translatedAttributes = ["text"];


}