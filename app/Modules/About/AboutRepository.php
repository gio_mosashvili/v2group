<?php
namespace App\Modules\About;

use BetterFly\Skeleton\Repositories\BaseRepository;

class AboutRepository extends BaseRepository {
    public function __construct(About $model){
        parent::__construct($model);
    }
}