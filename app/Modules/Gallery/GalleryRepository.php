<?php
namespace App\Modules\Gallery;

use BetterFly\Skeleton\Repositories\BaseRepository;

class GalleryRepository extends BaseRepository {
    public function __construct(Gallery $model){
        parent::__construct($model);
    }
}