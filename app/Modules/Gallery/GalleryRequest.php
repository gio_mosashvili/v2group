<?php
namespace App\Modules\Gallery;

use BetterFly\Skeleton\App\Http\Requests\BaseFormRequest;

class GalleryRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return $this->getRule();
    }

    /**
     * Custom message for validation
     *
     * @return  array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return  array
     */
    public function filters()
    {
        return [

        ];
    }

    private function getRule()
    {
        $type = $this->getMethod();

        switch($type)
        {
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                            'images' => 'required|string',
                        ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                            'images' => 'required|string',
                        ];
                }
            default:break;
        }
    }
}

