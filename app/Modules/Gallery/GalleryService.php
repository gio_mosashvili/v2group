<?php
namespace App\Modules\Gallery;


use BetterFly\Skeleton\Services\BaseService;

class GalleryService extends BaseService{

  public function __construct(GalleryRepository $repository){
    parent::__construct($repository);
  }
}