<?php
namespace App\Modules\Gallery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class GalleryController extends Controller
{
    protected $galleryService;

    public function __construct(GalleryService $galleryService)
    {
        $this->galleryService = $galleryService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

                $data = $this->galleryService->getList(['paginate' => 10]);
                if($request->ajax())
            return $data;
        else
            return view('admin.gallery.index')->with([
                'data' => $data,
                
]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.gallery.create')->with([
        
    ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @return  \Illuminate\Http\Response
    */
    public function store(GalleryRequest $request){

                $item = $this->galleryService->create($request->input());
        if($request->ajax()){
            return $item;
        }else{
            \Session::flash('status','Successfully created');
            return redirect()->route('gallery.index');
        }
    }

    /**
    * Display the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function show($id){
        return $this->galleryService->getById($id) ?:
        response(['message' => 'Record not found']);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function edit(Request $request,$id){
        $data = $this->galleryService->getById($id);
        if($request->ajax())
            return $data;
        else
            return view('admin.gallery.edit')->with([
                'data' => $data,
    
        
        ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function update(GalleryRequest $request, $id){
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->galleryService->update($data);

        if($request->ajax()){
            return $data;
        }else{
            \Session::flash('status','Successfully updated');
            return redirect()->route('gallery.edit',[$data->id]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return $this->galleryService->delete($id) ?
        response(['message' => 'Successfully deleted']) :
        response(['message' => 'Something went wrong']);
    }
}