<?php
namespace App\Modules\Gallery;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class GalleryTransformer extends BaseTransformerAbstract
{
    public function transform($gallery)
    {
        $data = [
            'id' => $gallery->id,
            'images' => $gallery->images,
        ];

        return $data;
    }
}