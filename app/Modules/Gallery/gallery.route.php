<?php
Route::resource('gallery', 'App\Modules\Gallery\GalleryController', [
    'names' => [
        'index' => 'gallery.index',
        'store' => 'gallery.store',
        'update' => 'gallery.update',
        'destroy' => 'gallery.delete'
    ]
]);
