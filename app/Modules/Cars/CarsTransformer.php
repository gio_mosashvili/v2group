<?php
namespace App\Modules\Cars;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class CarsTransformer extends BaseTransformerAbstract
{
    public function transform($cars)
    {
        $data = [
            'id' => $cars->id,
            'name' => $cars->name,
            'text' => $cars->text,
            'outer_image' => $cars->outer_image,
            'inner_image' => $cars->inner_image,
            'amenities' => $cars->amenities,
            'model' => $cars->model,
            'price' => $cars->price,
            'pasengers' => $cars->pasengers,
            'luggage' => $cars->luggage,
        ];

        return $data;
    }
}