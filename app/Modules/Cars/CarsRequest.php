<?php
namespace App\Modules\Cars;

use BetterFly\Skeleton\App\Http\Requests\BaseFormRequest;

class CarsRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return $this->getRule();
    }

    /**
     * Custom message for validation
     *
     * @return  array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return  array
     */
    public function filters()
    {
        return [

        ];
    }

    private function getRule()
    {
        $type = $this->getMethod();

        switch($type)
        {
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                            'name' => 'nullable|string',
                            'text' => 'nullable|string',
                            'outer_image' => 'nullable|string',
                            'inner_image' => 'nullable|string',
                            'amenities_id' => 'nullable|string',
                            'model' => 'nullable|max:255',
                            'price' => 'nullable|max:255',
                            'pasengers' => 'nullable|max:255',
                            'luggage' => 'nullable|max:255',
                        ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                            'name' => 'nullable|string',
                            'text' => 'nullable|string',
                            'outer_image' => 'nullable|string',
                            'inner_image' => 'nullable|string',
                            'amenities_id' => 'nullable|string',
                            'model' => 'nullable|max:255',
                            'price' => 'nullable|max:255',
                            'pasengers' => 'nullable|max:255',
                            'luggage' => 'nullable|max:255',
                        ];
                }
            default:break;
        }
    }
}

