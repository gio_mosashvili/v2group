<?php
namespace App\Modules\Cars;


use BetterFly\Skeleton\Services\BaseService;

class CarsService extends BaseService{

  public function __construct(CarsRepository $repository){
    parent::__construct($repository);
  }
}