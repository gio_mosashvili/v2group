<?php
namespace App\Modules\Cars;

    
use Illuminate\Database\Eloquent\Model;

class CarsTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["name","text",];


}