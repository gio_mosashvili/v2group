<?php
namespace App\Modules\Cars;

use BetterFly\Skeleton\Repositories\BaseRepository;

class CarsRepository extends BaseRepository {
    public function __construct(Cars $model){
        parent::__construct($model);
    }
}