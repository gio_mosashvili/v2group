<?php

namespace App\Modules\Cars;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    protected $table = 'cars';
    protected $fillable = ["days", "outer_image", "inner_image", "amenities_id", "model", "price", "pasengers", "luggage"];

    public function amenities()
    {
        return $this->belongsToMany('App\Modules\Amenities\Amenities', 'amenities_to_cars', 'car_id', 'amenity_id');
    }

    use Translatable;

    public $translationModel = 'App\Modules\Cars\CarsTranslation';
    public $translatedAttributes = ["name", "text",];


}