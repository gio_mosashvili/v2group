<?php
namespace App\Modules\Cars;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CarsController extends Controller
{
    protected $carsService;

    public function __construct(CarsService $carsService)
    {
        $this->carsService = $carsService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

                $data = $this->carsService->getList(['paginate' => 10]);
                if($request->ajax())
            return $data;
        else
            return view('admin.cars.index')->with([
                'data' => $data,
                
]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.cars.create')->with([
        
    ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @return  \Illuminate\Http\Response
    */
    public function store(CarsRequest $request){

                $item = $this->carsService->create($request->input());
        if($request->ajax()){
            return $item;
        }else{
            \Session::flash('status','Successfully created');
            return redirect()->route('cars.index');
        }
    }

    /**
    * Display the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function show($id){
        return $this->carsService->getById($id) ?:
        response(['message' => 'Record not found']);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function edit(Request $request,$id){
        $data = $this->carsService->getById($id);
        if($request->ajax())
            return $data;
        else
            return view('admin.cars.edit')->with([
                'data' => $data,
    
        
        ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function update(CarsRequest $request, $id){
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->carsService->update($data);

        if($request->ajax()){
            return $data;
        }else{
            \Session::flash('status','Successfully updated');
            return redirect()->route('cars.edit',[$data->id]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return $this->carsService->delete($id) ?
        response(['message' => 'Successfully deleted']) :
        response(['message' => 'Something went wrong']);
    }
}