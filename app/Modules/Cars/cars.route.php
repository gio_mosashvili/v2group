<?php
Route::resource('cars', 'App\Modules\Cars\CarsController', [
    'names' => [
        'index' => 'cars.index',
        'store' => 'cars.store',
        'update' => 'cars.update',
        'destroy' => 'cars.delete'
    ]
]);
