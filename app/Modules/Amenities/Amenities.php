<?php
namespace App\Modules\Amenities;

        use Astrotomic\Translatable\Translatable;
            use Kalnoy\Nestedset\NodeTrait;
    
use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    protected $table = 'amenities';
        use NodeTrait;
            protected $fillable = ["id"];

        use Translatable;

    public $translationModel = 'App\Modules\Amenities\AmenitiesTranslation';
    public $translatedAttributes = ["name","description", ];


}