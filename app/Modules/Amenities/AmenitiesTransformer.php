<?php
namespace App\Modules\Amenities;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class AmenitiesTransformer extends BaseTransformerAbstract
{
    public function transform($amenities)
    {
        $data = [
            'id' => $amenities->id,
            'name' => $amenities->name,
            'description' => $amenities->description,
        ];

        return $data;
    }
}