<?php
namespace App\Modules\Amenities;

    
use Illuminate\Database\Eloquent\Model;

class AmenitiesTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["name","description",];


}