<?php
namespace App\Modules\Amenities;

use BetterFly\Skeleton\Repositories\BaseRepository;

class AmenitiesRepository extends BaseRepository {
    public function __construct(Amenities $model){
        parent::__construct($model);
    }
}