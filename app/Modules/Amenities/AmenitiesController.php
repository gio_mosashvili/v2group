<?php
namespace App\Modules\Amenities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AmenitiesController extends Controller
{
    protected $amenitiesService;

    public function __construct(AmenitiesService $amenitiesService)
    {
        $this->amenitiesService = $amenitiesService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

                $data = $this->amenitiesService->getList(['paginate' => 10])->toTree();
                if($request->ajax())
            return $data;
        else
            return view('admin.amenities.index')->with([
                'data' => $data,
                
]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.amenities.create')->with([
        
    ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @return  \Illuminate\Http\Response
    */
    public function store(AmenitiesRequest $request){

                $item = $this->amenitiesService->create($request->input());
        if($request->ajax()){
            return $item;
        }else{
            \Session::flash('status','Successfully created');
            return redirect()->route('amenities.index');
        }
    }

    /**
    * Display the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function show($id){
        return $this->amenitiesService->getById($id) ?:
        response(['message' => 'Record not found']);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function edit(Request $request,$id){
        $data = $this->amenitiesService->getById($id);
        if($request->ajax())
            return $data;
        else
            return view('admin.amenities.edit')->with([
                'data' => $data,
    
        
        ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function update(AmenitiesRequest $request, $id){
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->amenitiesService->update($data);

        if($request->ajax()){
            return $data;
        }else{
            \Session::flash('status','Successfully updated');
            return redirect()->route('amenities.edit',[$data->id]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return $this->amenitiesService->delete($id) ?
        response(['message' => 'Successfully deleted']) :
        response(['message' => 'Something went wrong']);
    }
}