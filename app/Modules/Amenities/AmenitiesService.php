<?php
namespace App\Modules\Amenities;


use BetterFly\Skeleton\Services\BaseService;

class AmenitiesService extends BaseService{

  public function __construct(AmenitiesRepository $repository){
    parent::__construct($repository);
  }
}