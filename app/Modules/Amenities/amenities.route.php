<?php
Route::resource('amenities', 'App\Modules\Amenities\AmenitiesController', [
    'names' => [
        'index' => 'amenities.index',
        'store' => 'amenities.store',
        'update' => 'amenities.update',
        'destroy' => 'amenities.delete'
    ]
]);
