<?php
namespace App\Modules\Tours;


use BetterFly\Skeleton\Services\BaseService;

class ToursService extends BaseService{

  public function __construct(ToursRepository $repository){
    parent::__construct($repository);
  }
}