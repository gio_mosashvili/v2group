<?php
Route::resource('tours', 'App\Modules\Tours\ToursController', [
    'names' => [
        'index' => 'tours.index',
        'store' => 'tours.store',
        'update' => 'tours.update',
        'destroy' => 'tours.delete'
    ]
]);
