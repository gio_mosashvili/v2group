<?php
namespace App\Modules\Tours;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ToursController extends Controller
{
    protected $toursService;

    public function __construct(ToursService $toursService)
    {
        $this->toursService = $toursService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

                $data = $this->toursService->getList(['paginate' => 10]);
                if($request->ajax())
            return $data;
        else
            return view('admin.tours.index')->with([
                'data' => $data,
                
]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.tours.create')->with([
        
    ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @return  \Illuminate\Http\Response
    */
    public function store(ToursRequest $request){

                $item = $this->toursService->create($request->input());
        if($request->ajax()){
            return $item;
        }else{
            \Session::flash('status','Successfully created');
            return redirect()->route('tours.index');
        }
    }

    /**
    * Display the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function show($id){
        return $this->toursService->getById($id) ?:
        response(['message' => 'Record not found']);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function edit(Request $request,$id){
        $data = $this->toursService->getById($id);
        if($request->ajax())
            return $data;
        else
            return view('admin.tours.edit')->with([
                'data' => $data,
    
        
        ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param    \Illuminate\Http\Request $request
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function update(ToursRequest $request, $id){
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->toursService->update($data);

        if($request->ajax()){
            return $data;
        }else{
            \Session::flash('status','Successfully updated');
            return redirect()->route('tours.edit',[$data->id]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param    int $id
    * @return  \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return $this->toursService->delete($id) ?
        response(['message' => 'Successfully deleted']) :
        response(['message' => 'Something went wrong']);
    }
}