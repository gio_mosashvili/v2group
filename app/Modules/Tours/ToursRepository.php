<?php
namespace App\Modules\Tours;

use BetterFly\Skeleton\Repositories\BaseRepository;

class ToursRepository extends BaseRepository {
    public function __construct(Tours $model){
        parent::__construct($model);
    }
}