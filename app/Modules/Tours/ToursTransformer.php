<?php
namespace App\Modules\Tours;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class ToursTransformer extends BaseTransformerAbstract
{
    public function transform($tours)
    {
        $data = [
            'id' => $tours->id,
            'name' => $tours->name,
            'text' => $tours->text,
            'outer_image' => $tours->outer_image,
            'inner_image' => $tours->inner_image,
            'amenities_id' => $tours->amenities_id,
            'days' => $tours->days,
        ];

        return $data;
    }
}