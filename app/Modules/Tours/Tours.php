<?php

namespace App\Modules\Tours;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class Tours extends Model
{
    protected $table = 'tours';
    protected $fillable = ["outer_image", "inner_image", "amenities_id"];

    public function amenities()
    {
        return $this->belongsToMany('App\Modules\Amenities\Amenities', 'amenities_to_tours', 'tour_id', 'amenity_id');
    }

    use Translatable;

    public $translationModel = 'App\Modules\Tours\ToursTranslation';
    public $translatedAttributes = ["days", "name", "text",];


}