<?php
namespace App\Modules\Tours;

    
use Illuminate\Database\Eloquent\Model;

class ToursTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["days","name","text",];


}